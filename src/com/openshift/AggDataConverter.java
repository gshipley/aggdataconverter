package com.openshift;

import java.io.*;

public class AggDataConverter {

    public static void main(String[] args) {
        int foundHeadings = 0;
        String[] headings = null;
        FileWriter fw = null;
        BufferedWriter bw = null;
        int lineNumber = 1;
        if (args.length < 1) {
            System.out.println("Usage: java AggDataConverter file.csv");
            System.exit(0);
        }
        try {
            fw = new FileWriter(new File("out.json").getAbsoluteFile());
            bw = new BufferedWriter(fw);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            BufferedReader br = new BufferedReader(new FileReader(args[0]));
            String line;

            while ((line = br.readLine()) != null) {
                if (foundHeadings == 0) {
                    foundHeadings = 1;
                    headings = line.split(",");
                } else {
                    int theLong =0;
                    int theLat = 0;
                        // Now we need to create our json file
                        bw.write("{ ");
                    for (int i = 0; i < headings.length; i++) {
                        if(line.startsWith(",")) {
                            line = line.substring(1, line.length());
                        }
                        String[] currentData = line.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)", -1);
                        //remove quotes if included
                        if (currentData[i].contains("\"")) {
                            currentData[i] = currentData[i].replaceAll("\"", "");
                        }


                            bw.write("\"" + headings[i] + "\": \"" + currentData[i] + "\"");
                            if (i < headings.length - 1) {
                                bw.write((","));
                            } else {
                                //we need to write our pos field
                                for(int x=0;x<headings.length; x++) {

                                    if(headings[x].equals("Longitude")) {
                                        theLong = x;
                                    } else if (headings[x].equals("Latitude")) {
                                        theLat = x;
                                    }
                                }
                                //"pos": [-147.7134667, 64.8156796]
                                bw.write(", \"pos\": [" + currentData[theLong] + ", " + currentData[theLat] + "]");
                                bw.write("}");
                            }

                    }
                    bw.newLine();
                }


            }
            bw.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
